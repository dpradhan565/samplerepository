package stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MapUse {

	
	public static void main(String[] args) {
		List<Integer> list=Arrays.asList(2,4,8);
		List<Integer> collect = list.stream().map(x->x*x).collect(Collectors.toList());
		collect.stream().forEach(y->System.out.print(y));
		

	}

}
